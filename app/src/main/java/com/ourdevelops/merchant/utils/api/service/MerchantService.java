package com.ourdevelops.merchant.utils.api.service;

import com.ourdevelops.merchant.json.ActivekatRequestJson;
import com.ourdevelops.merchant.json.AddEditItemRequestJson;
import com.ourdevelops.merchant.json.AddEditKategoriRequestJson;
import com.ourdevelops.merchant.json.BankResponseJson;
import com.ourdevelops.merchant.json.ChangePassRequestJson;
import com.ourdevelops.merchant.json.DetailRequestJson;
import com.ourdevelops.merchant.json.DetailTransResponseJson;
import com.ourdevelops.merchant.json.EditMerchantRequestJson;
import com.ourdevelops.merchant.json.EditProfileRequestJson;
import com.ourdevelops.merchant.json.GetFiturResponseJson;
import com.ourdevelops.merchant.json.GetOnRequestJson;
import com.ourdevelops.merchant.json.HistoryRequestJson;
import com.ourdevelops.merchant.json.HistoryResponseJson;
import com.ourdevelops.merchant.json.HomeRequestJson;
import com.ourdevelops.merchant.json.HomeResponseJson;
import com.ourdevelops.merchant.json.ItemRequestJson;
import com.ourdevelops.merchant.json.ItemResponseJson;
import com.ourdevelops.merchant.json.KategoriRequestJson;
import com.ourdevelops.merchant.json.KategoriResponseJson;
import com.ourdevelops.merchant.json.LoginRequestJson;
import com.ourdevelops.merchant.json.LoginResponseJson;
import com.ourdevelops.merchant.json.PrivacyRequestJson;
import com.ourdevelops.merchant.json.PrivacyResponseJson;
import com.ourdevelops.merchant.json.RegisterRequestJson;
import com.ourdevelops.merchant.json.RegisterResponseJson;
import com.ourdevelops.merchant.json.ResponseJson;
import com.ourdevelops.merchant.json.TopupRequestJson;
import com.ourdevelops.merchant.json.TopupResponseJson;
import com.ourdevelops.merchant.json.WalletRequestJson;
import com.ourdevelops.merchant.json.WalletResponseJson;
import com.ourdevelops.merchant.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface MerchantService {

    @GET("merchant/kategorimerchant")
    Call<GetFiturResponseJson> getFitur();

    @POST("pelanggan/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("merchant/kategorimerchantbyfitur")
    Call<GetFiturResponseJson> getKategori(@Body HistoryRequestJson param);

    @POST("merchant/onoff")
    Call<ResponseJson> turnon(@Body GetOnRequestJson param);

    @POST("merchant/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("merchant/register_merchant")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @POST("merchant/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("pelanggan/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("merchant/edit_profile")
    Call<LoginResponseJson> editprofile(@Body EditProfileRequestJson param);

    @POST("merchant/edit_merchant")
    Call<LoginResponseJson> editmerchant(@Body EditMerchantRequestJson param);

    @POST("merchant/home")
    Call<HomeResponseJson> home(@Body HomeRequestJson param);

    @POST("merchant/history")
    Call<HistoryResponseJson> history(@Body HistoryRequestJson param);

    @POST("merchant/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

    @POST("merchant/kategori")
    Call<KategoriResponseJson> kategori(@Body KategoriRequestJson param);

    @POST("merchant/item")
    Call<ItemResponseJson> itemlist(@Body ItemRequestJson param);

    @POST("merchant/active_kategori")
    Call<ResponseJson> activekategori(@Body ActivekatRequestJson param);

    @POST("merchant/active_item")
    Call<ResponseJson> activeitem(@Body ActivekatRequestJson param);

    @POST("merchant/add_kategori")
    Call<ResponseJson> addkategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/edit_kategori")
    Call<ResponseJson> editkategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/delete_kategori")
    Call<ResponseJson> deletekategori(@Body AddEditKategoriRequestJson param);

    @POST("merchant/add_item")
    Call<ResponseJson> additem(@Body AddEditItemRequestJson param);

    @POST("merchant/edit_item")
    Call<ResponseJson> edititem(@Body AddEditItemRequestJson param);

    @POST("merchant/delete_item")
    Call<ResponseJson> deleteitem(@Body AddEditItemRequestJson param);

    @POST("pelanggan/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("merchant/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("pelanggan/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("merchant/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("merchant/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

}
